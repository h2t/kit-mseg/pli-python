deb_dist = ./deb-dist

.depsdeb:
	@echo python
	@echo python-setuptools
	@echo python-stdeb

clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ./dist 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true
	@rm -r ./deb_dist 2> /dev/null || true
	@rm -r ./plipython.egg-info 2> /dev/null || true
	@# Deletes all installed files outside of the repository
	@xargs rm 2> /dev/null < .installed-files.log || true
	@rm .installed-files.log 2> /dev/null || true

install:
	@# installed files will be written into `.installed-files.log`
	python setup.py install --user --record .installed-files.log

package:
	@mkdir ${deb_dist} 2> /dev/null || true
	python setup.py --command-packages=stdeb.command bdist_deb
	@mv ./deb_dist/*.deb ${deb_dist}/
