"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import os
import signal

import Ice
import IceGrid

interrupt = False
ic = None

def shutdownIceConnection(signal, frame):

    global ic

    print ''
    
    try:

        ic.shutdown()

    except:

        pass

class AlgorithmServer:

    def serve(self, algorithm):

        global ic
        
        home = os.environ['HOME']
        parameters = ['--Ice.Config=' + home + '/.armarx/default.cfg']

        ic = Ice.initialize(parameters)

        # Add this algorithm as object to IceGrid
        adapter = ic.createObjectAdapterWithEndpoints(algorithm.name, 'tcp')
        adapter.activate()

        id = ic.stringToIdentity(algorithm.name)
        adapter.add(algorithm, id)

        proxy = adapter.createProxy(id)

        # Try to connect to ArmarX' IceGrid
        try:

            regObj = ic.stringToProxy('IceGrid/Registry')
            reg = IceGrid.RegistryPrx.checkedCast(regObj)

            adminSession = reg.createAdminSession('user', 'password')
            admin = adminSession.getAdmin()

            # Try to add algorithm to ArmarX' IceGrid
            try:

                admin.addObjectWithType(proxy, proxy.ice_id())

            # Retry to add algorithm if it already exists
            except IceGrid.ObjectExistsException:
            
                # If adding the object fails because of the exception above, there is a stray object from a previous instance...
                print 'Algorithm with the same name already registered. Perhaps a previous instance crashed. Cleaning up now...'

                # ...therefore the stray object will be removed...
                admin.removeObject(id)

                print 'Retrying...'

                # ...and the addition of the object to ArmarX' IceGrid will be retried
                admin.addObjectWithType(proxy, proxy.ice_id())

            # Close session
            adminSession.destroy()

            # Try to connect to DataExchange
            try:

                print 'Connecting to MSeg core module...'

                self.initData(ic, algorithm)

                print 'Connection established'

                # Indicate that initialization finished successfully and waiting for instructions
                print algorithm.name + ' ready'

                signal.signal(signal.SIGINT, shutdownIceConnection)

                # Wait for SIGINT or shutdown
                ic.waitForShutdown()
                    
            # Indicate that user has to start mseg if not registered
            except Ice.NotRegisteredException:
            
                print 'MSeg core module not available. Start it by running `msegcm start`'
            
            print 'Cleaning up...'

            adminSession = reg.createAdminSession('user', 'password')
            admin = adminSession.getAdmin()

            admin.removeObject(id)

            adminSession.destroy()
        
        # Indicate that user has to start armarx
        except Ice.ConnectionRefusedException:
        
            print 'ArmarX is not available. Start it by running \'armarx start\''

        if ic is not None:
        
            ic.destroy()
    
    def initData(self, ic, algorithm):

        algorithm.data.connect(ic)
